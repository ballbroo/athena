#include "TrigT1CTP/CTPSimulation.h"
#include "TrigT1CTP/CTPDataDumper.h"

using LVL1CTP::CTPSimulation;
using LVL1CTP::CTPDataDumper;

DECLARE_COMPONENT( CTPSimulation )
DECLARE_COMPONENT( CTPDataDumper )
